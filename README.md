# Seppiko Vault

Seppiko Vault is a storage service with RESTful API for Amazon S3 storage protocol

## Configuration

You need `vaultconfig.yml` OR `vaultconfig.yaml` in `vault.jar` folder
```
-Dfile.encoding=UTF-8
```

OR:
```
-Dpigeon.configFile=./vaultconfig.yml
-Dlog4j.configurationFile=./log4j2.xml
-Duser.timezone=UTC
```

## API

### `/upload` POST

Upload JPEG or PNG image

Demo:
```html
<form enctype="multipart/form-data" action="http://localhost:8080/upload" method="POST">
Choose file: <input name="file" type="file" />
<input type="submit" value="Send File" />
</form>
```

If you upload success, you will get a json like `{"status":200, "message":"/vault/FILENAME.jpg"}`

And the MAX image size is `5MB`

### `/vault/{name}` GET

Get JPEG image

## License
This project is released under [Apache License,Version 2.0][alv2].

[alv2]: https://www.apache.org/licenses/LICENSE-2.0
