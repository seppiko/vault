/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.vault.configure;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Objects;
import org.seppiko.vault.utils.LoggingManager;

/**
 * @author Leonard Woo
 */
public class VaultConfiguration {

  private static VaultConfiguration instance = new VaultConfiguration();
  public static VaultConfiguration getInstance() {
    return instance;
  }
  private VaultConfiguration() {
    initConfig();
    init();
  }

  private LoggingManager log = LoggingManager.getInstance().getLogger(this.getClass());

  private final String CONFIGURATION_FILE = "configFile";
  private final String VAULT_CONFIG_NAME_YAML = "vaultconfig.yaml";
  private final String VAULT_CONFIG_NAME_YML = "vaultconfig.yml";

  private final ObjectMapper yamlMapper = new ObjectMapper(new YAMLFactory());

  private ConfigEntity config;

  private String bucket;

  public ConfigEntity getConfig() {
    return config;
  }

  public String getBucket() {
    return bucket;
  }

  private void initConfig() {
    yamlMapper.configure(DeserializationFeature. FAIL_ON_UNKNOWN_PROPERTIES, false);
  }

  private void init() {
    String filepath = System.getProperty(CONFIGURATION_FILE, VAULT_CONFIG_NAME_YAML);
    try {
      InputStream is = getPath(filepath);
      if (is == null) {
        is = getPath(VAULT_CONFIG_NAME_YML);
      }
      if(is == null) {
        throw new FileNotFoundException(VAULT_CONFIG_NAME_YAML + " not found");
      }
      BufferedReader reader = loadReader(is);
      JsonNode vaultJson = yamlMapper.readTree(reader).get("vault");
      log.info("Seppiko Vault Configure: " + vaultJson.toString());

      this.config = yamlMapper.convertValue(vaultJson, ConfigEntity.class);
      this.bucket = vaultJson.get("bucket").asText();

    } catch (IOException e) {
      log.error("Load config failed.", e);
    }
  }

  private BufferedReader loadReader(InputStream is) {
    return new BufferedReader( new InputStreamReader( new BufferedInputStream( is ) ) );
  }

  private InputStream getPath(String name) {
    try {
      ClassLoader cl = this.getClass().getClassLoader();
      InputStream is = cl.getResourceAsStream(name);
      if (is == null) {
        try {
          is = new FileInputStream(
              Objects.requireNonNull(cl.getResource(name)).getPath());
        } catch (NullPointerException ex) {
          is = null;
        }
      }
      if (is == null) {
        is = new FileInputStream(name);
      }
      return is;
    } catch (IOException ex) {
      log.warn(ex.getMessage());
      return null;
    }
  }
}
