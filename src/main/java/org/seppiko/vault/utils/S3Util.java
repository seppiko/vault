/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.vault.utils;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import org.seppiko.vault.configure.ConfigEntity;
import org.seppiko.vault.configure.VaultConfiguration;

/**
 * AWS S3 client util
 *
 * @author Leonard Woo
 */
public class S3Util {

  private static S3Util instance = new S3Util();
  public static S3Util getInstance() {
    return instance;
  }
  private S3Util(){
    init();
  }

  private VaultConfiguration configuration = VaultConfiguration.getInstance();

  private LoggingManager log = LoggingManager.getInstance().getLogger(this.getClass());

  private AmazonS3 s3Client;

  private ConfigEntity config = configuration.getConfig();
  private String bucket = configuration.getBucket();
  private boolean isExists = false;

  private void init() {
    AWSCredentials credentials = new BasicAWSCredentials(config.getAccessKey(), config.getSecretKey());

    ClientConfiguration clientConfiguration = new ClientConfiguration();
    clientConfiguration.setSignerOverride("AWSS3V4SignerType");

    s3Client = AmazonS3ClientBuilder
        .standard()
        .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(config.getUrl(), config.getRegions()))
        .withPathStyleAccessEnabled(true)
        .withClientConfiguration(clientConfiguration)
        .withCredentials(new AWSStaticCredentialsProvider(credentials))
        .build();

    bucketCheck(bucket);
  }

  public void saveFile(byte[] bytes, String name) {
    if(!isExists) {
      s3Client.createBucket(bucket);
    }
    ObjectMetadata objectMetadata = new ObjectMetadata();
    objectMetadata.setContentLength(bytes.length);
    objectMetadata.setContentType("application/octet-stream");
    s3Client.putObject(new PutObjectRequest(bucket, name, new ByteArrayInputStream(bytes), objectMetadata));
  }

  private void bucketCheck(String bucketName) {
    s3Client.listBuckets().forEach(bucket1 -> {
      isExists = bucket1.getName().equals(bucketName);
    });
  }

  public byte[] loadFile(String name) {
    try {
      if(!isExists) {
        return null;
      }
      return s3Client.getObject(new GetObjectRequest(bucket, name)).getObjectContent().readAllBytes();
    } catch (IOException e) {
      log.warn("File not found.", e);
      return null;
    }
  }
}
