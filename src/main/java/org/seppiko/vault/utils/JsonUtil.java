/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.vault.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Leonard Woo
 */
public class JsonUtil {

  private static LoggingManager log = LoggingManager.getInstance().getLogger(JsonUtil.class);

  public static String toJson(Object jsonObject) {
    ObjectMapper mapper = new ObjectMapper();
    try {
      return mapper.writeValueAsString(jsonObject);
    } catch (JsonProcessingException e) {
      log.warn("Json string generator exception.", e);
    }
    return "{}";
  }
}
