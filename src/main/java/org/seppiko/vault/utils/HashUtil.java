/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.vault.utils;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * @author Leonard Woo
 */
public class HashUtil {

  public static String inputStreamHash(byte[] bytes) {
    byte[] hash = getSha256(bytes);
    String target =  Base36.encode(hash);
    target = target.endsWith("=") ? target.substring(0, target.lastIndexOf("=") - 1) : target;
    return target;
  }

  public static byte[] getSha256(byte[] bytes) {
    return DigestUtils.sha256(bytes);
  }
}
