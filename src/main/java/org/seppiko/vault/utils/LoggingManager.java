/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.vault.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Leonard Woo
 */
public class LoggingManager {

  private static LoggingManager INSTANCE = new LoggingManager();

  public static LoggingManager getInstance() {
    return INSTANCE;
  }

  private LoggingManager() {
  }

  public LoggingManager getLogger(Class<?> clazz) {
    return getLogger(clazz.getName());
  }

  private Logger logger;

  public LoggingManager getLogger(String name) {
    this.logger = LoggerFactory.getLogger(name);
    return this;
  }

  public void debug(String message) {
    logger.debug(message);
  }

  public void info(String message) {
    logger.info(message);
  }

  public void warn(String message) {
    logger.warn(message);
  }

  public void warn(String message, Throwable cause) {
    logger.warn(message, cause);
  }

  public void error(String message) {
    logger.error(message);
  }

  public void error(String message, Throwable cause) {
    logger.error(message, cause);
  }
}
